# Offer Me platform

Technologies: `Python 3.12 Django 4.2.6 Postgresql 16.0`

Platform used to post job offers with user account management system

### Preparing project

- Copy `.env.dev` to `.env` (and change configuration if necessary)
- Execute `docker-compose build`
- Execute `docker-compose up -d`

### WORK IN PROGRESS
