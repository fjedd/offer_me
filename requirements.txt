django==4.2.6
pre-commit==3.5.0
psycopg2>=2.9.9
python-dotenv==1.0.0
